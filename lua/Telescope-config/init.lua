require('telescope').setup {
	defaults = {
		layout_strategy = 'horizontal',
		layout_config = { height = 0.50, prompt_position = "top" },
		scroll_strategy = 'limit',
		border = true,
		path_dispaly = 'absolute',
		dynamic_preview_title = true,
		prompt_prefix = "> ",
		selection_caret = "> ",
		sorting_strategy = "ascending",
		color_devicons = true,

	},
	pickers = {
		find_files = {
			theme = "ivy",
			layout_strategy = 'horizontal',
			layout_config = { height = 0.50, prompt_position = "top", previewe_width=0.8 },

		},

		git_files = {
			theme =  "ivy",
			layout_strateg = 'horizontal',
			layout_config = { height = 0.50, prompt_position = "top", previewe_width=0.8 },
		}

	},

	extensions = {
		file_browser  = {
			theme = "ivy",
			hijack_netrw = true,
			hidden = true,
		},
		repo =  {
			list = {
				fd_opts = {
					"--no-ignore-vcs",
				},
				search_dirs =  {
				  "~/projects", "~/git",
				},
			},
		},

	},

}

require("telescope").load_extension "repo"
require("telescope").load_extension "file_browser"
