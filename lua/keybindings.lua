keymap = vim.api.nvim_set_keymap
local opts   = { noremap = true, silent = false }

vim.g.mapleader  = ' '
-- vim.keymap.set("n", "<leader>j", ":wincmd j<CR>") 
-- vim.keymap.set("n", "<leader>k", ":wincmd k<CR>")

-- window moving 
keymap("n", "<leader>j", ":wincmd j<CR>", opts)
keymap("n", "<leader>k", ":wincmd k<CR>", opts)
keymap("n", "<leader>l", ":wincmd l<CR>", opts)
keymap("n", "<leader>h", ":wincmd h<CR>", opts)

-- swap windows
keymap("n", "<leader>J", ":wincmd J<CR>", opts)
keymap("n", "<leader>K", ":wincmd K<CR>", opts)
keymap("n", "<leader>L", ":wincmd L<CR>", opts)
keymap("n", "<leader>H", ":wincmd H<CR>", opts)

keymap("n", "+h", ":resize +5<CR>", opts)
keymap("n", "+w", ":vertical resize +5<CR>", opts)
keymap("n", "-h", ":resize -5<CR>", opts)
keymap("n", "-w", ":vertical resize -5<CR>", opts)

--buffer switching
keymap("n", "bn", ":bnext<CR>", opts)
keymap("n", "bp", ":bprev<CR>", opts)
keymap("n", "bd", ":bd<CR>", opts)

--telescope          
keymap("n", "tf", ":Telescope find_files<CR>", opts)
keymap("n", "tv", ":Telescope live_grep<CR>", opts)
keymap("n", "tb", ":Telescope buffers<CR>", opts)
keymap("n", "th", ":Telescope help_tags<CR>", opts)
keymap("n", "tg", ":Telescope git_files<CR>", opts)
--neovim
--keymap("n", "/", ":Neotree toggle current reveal_force_cwd<CR>", opts)



--nnoremap / :Neotree toggle current reveal_force_cwd<cr>
--nnoremap | :Neotree reveal<cr>
--nnoremap gd :Neotree float reveal_file=<cfile> reveal_force_cwd<cr>
--nnoremap <leader>b :Neotree toggle show buffers right<cr>
--nnoremap <leader>s :Neotree float git_status<cr>


